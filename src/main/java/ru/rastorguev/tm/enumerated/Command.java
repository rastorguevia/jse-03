package ru.rastorguev.tm.enumerated;

public enum Command {
    EMPTY,
    HELP,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_SELECT,
    PROJECT_EDIT,
    PROJECT_REMOVE,
    PROJECT_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_SELECT,
    TASK_EDIT,
    TASK_REMOVE,
    TASK_CLEAR,
    UNKNOWN,
    EXIT,
    ALL,
    Y,
    N
}
