package ru.rastorguev.tm;

import static ru.rastorguev.tm.view.View.*;
import ru.rastorguev.tm.controller.CommandController;
import ru.rastorguev.tm.enumerated.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        CommandController commandController = new CommandController(reader);

        showWelcomeMsg();

        while(true) {
            Command command = Command.EMPTY;
            String input = reader.readLine();
            try {
                command = Command.valueOf(input.toUpperCase());
            } catch (IllegalArgumentException e) {
                commandController.commandExecution(Command.UNKNOWN);
            }
            if (Command.EXIT.equals(command)) {
                reader.close();
                System.exit(0);
            }
            commandController.commandExecution(command);
        }
    }
}