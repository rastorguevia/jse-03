package ru.rastorguev.tm.entity;

import java.util.Date;
import java.util.UUID;

import static ru.rastorguev.tm.util.DateUtil.*;

public class AbstractEntity {
    private final String id = UUID.randomUUID().toString();
    private String name = "";
    private String description = "";
    private Date startDate = stringToDate(dateFormatter.format(new Date()));
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
