package ru.rastorguev.tm.entity;

public class Task extends AbstractEntity {
    private final String projectId;

    public Task(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }
}