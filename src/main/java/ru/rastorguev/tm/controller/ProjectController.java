package ru.rastorguev.tm.controller;

import static ru.rastorguev.tm.util.DateUtil.*;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.enumerated.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class ProjectController {
    private final BufferedReader reader;
    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    public Map<String, Project> getProjectMap() {
        return projectMap;
    }

    public ProjectController(BufferedReader reader) {
        this.reader = reader;
    }

    public void createProject() throws IOException {
        System.out.println("Project create");
        System.out.println("Enter name");
        Project project = new Project();
        project.setName(reader.readLine());
        System.out.println("Enter description");
        project.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                project.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                project.setEndDate(stringToDate(reader.readLine()));
            }
        }
        projectMap.put(project.getId(), project);
        System.out.println("OK");
    }

    public void listProjects() {
        System.out.println("Project list");
        printAllProjects(projectMap);
        System.out.println("OK");
    }

    public void selectProject() throws IOException {
        System.out.println("Project select");
        System.out.println("Enter project ID");
        printAllProjects(projectMap);
        printProject(getProjectByNumber());
        System.out.println("OK");
    }

    public void editProject() throws IOException {
        System.out.println("Project edit");
        System.out.println("Enter project ID");
        printAllProjects(projectMap);
        Project project = getProjectByNumber();

        System.out.println("Edit name? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter project name");
            project.setName(reader.readLine());
        }
        System.out.println("Edit description? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            project.setDescription(reader.readLine());
        }
        System.out.println("Edit start date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            project.setStartDate(stringToDate(reader.readLine()));
        }
        System.out.println("Edit end date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            project.setEndDate(stringToDate(reader.readLine()));
        }
        System.out.println("OK");
    }

    public void removeProject() throws IOException {
        System.out.println("Project remove");
        System.out.println("Enter ID");
        printAllProjects(projectMap);
        String projectId = getIdByNumber(Integer.parseInt(reader.readLine()), projectMap);
        projectMap.remove(projectId);
        System.out.println("OK");
    }

    public void clearProjects() {
        projectMap.clear();
        System.out.println("OK");
    }

    public static void printAllProjects(Map <String, Project> projects) {
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projects.values());
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public void printProject(Project project) {
        System.out.println("Project name: " + project.getName() +
                "\nProject description: " + project.getDescription() +
                "\nProject start date: " + dateFormatter.format(project.getStartDate()) +
                "\nProject end date: " + dateFormatter.format(project.getEndDate()));
    }

    public Project getProjectByNumber() throws IOException {
        return projectMap.get(getIdByNumber(Integer.parseInt(reader.readLine()), projectMap));
    }

    public String getIdByNumber(int n, Map <String, Project> projects) {
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projects.values());
        for (Project project: listOfProjects) {
            if (project.getId().equals(listOfProjects.get(n - 1).getId())){
                return project.getId();
            }
        }
        return null;
    }
}