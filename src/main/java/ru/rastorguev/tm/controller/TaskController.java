package ru.rastorguev.tm.controller;

import static ru.rastorguev.tm.util.DateUtil.*;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TaskController {
    private final BufferedReader reader;
    private final Map<String, Task> taskMap = new LinkedHashMap<>();
    private ProjectController projectController;

    public TaskController(ProjectController projectController, BufferedReader reader) {
        this.projectController = projectController;
        this.reader = reader;
    }

    public void createTask() throws IOException {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        Task task = new Task(projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap()));
        System.out.println("Enter task name");
        task.setName(reader.readLine());
        System.out.println("Enter task description");
        task.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                task.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                task.setEndDate(stringToDate(reader.readLine()));
            }
        }
        taskMap.put(task.getId(), task);
        System.out.println("OK");
    }

    public void listTasks() throws IOException {
        System.out.println("Task list");
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        String projectId = projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap());
        printTaskListByProjectId(projectId, taskMap);
        System.out.println("OK");
    }

    public void selectTask() throws IOException {
        System.out.println("Task select");
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        String projectId = projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap());
        printTaskListByProjectId(projectId, taskMap);
        List<Task> filteredTaskList = filterTaskListByProjectId(projectId, taskMap);
        System.out.println("Enter Task ID");
        String taskId = getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        printTask(taskMap.get(taskId));
        System.out.println("OK");
    }

    public void editTask() throws IOException {
        System.out.println("Task edit");
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        String projectId = projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap());
        printTaskListByProjectId(projectId, taskMap);
        List<Task> filteredTaskList = filterTaskListByProjectId(projectId, taskMap);
        System.out.println("Enter Task ID");
        String taskId = getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        Task task = taskMap.get(taskId);
        System.out.println("Edit name? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter task name");
            task.setName(reader.readLine());
        }
        System.out.println("Edit description? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            task.setDescription(reader.readLine());
        }
        System.out.println("Edit start date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            task.setStartDate(stringToDate(reader.readLine()));
        }
        System.out.println("Edit end date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            task.setEndDate(stringToDate(reader.readLine()));
        }
        System.out.println("OK");
    }

    public void removeTask() throws IOException {
        System.out.println("Task remove");
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        String projectId = projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap());
        printTaskListByProjectId(projectId, taskMap);
        List<Task> filteredTaskList = filterTaskListByProjectId(projectId, taskMap);
        System.out.println("Enter Task ID");
        String taskId = getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        taskMap.remove(taskId);
        System.out.println("OK");
    }

    public void clearProjectTasks() throws IOException {
        System.out.println("Enter Project ID");
        projectController.printAllProjects(projectController.getProjectMap());
        String projectId = projectController.getIdByNumber(Integer.parseInt(reader.readLine()), projectController.getProjectMap());
        removeTaskListByProjectId(projectId);
        System.out.println("OK");
    }

    public List<Task> filterTaskListByProjectId(String projectId, Map<String, Task> tasks) {
        List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(tasks.values());
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (!listOfTasks.get(i).getProjectId().equals(projectId)) {
                listOfTasks.remove(i);
            }
        }
        return listOfTasks;
    }

    public void printTaskListByProjectId(String projectId, Map<String, Task> tasks) {
        List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(tasks.values());
        for (int i = 0; i < listOfTasks.size(); i++) {
             if (!listOfTasks.get(i).getProjectId().equals(projectId)) {
                 listOfTasks.remove(i);
             }
         }
        for (int i = 0; i < listOfTasks.size(); i++) {
            System.out.println((i + 1) + "." + listOfTasks.get(i).getName());
        }
    }

    public void printTask(Task task) {
        System.out.println("Task name: " + task.getName() +
                "\nTask description: " + task.getDescription() +
                "\nTask start date: " + dateFormatter.format(task.getStartDate()) +
                "\nTask end date: " + dateFormatter.format(task.getEndDate()));
    }

    public void removeTaskListByProjectId(String projectId) {
        List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskMap.values());
        for (Task task: listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                taskMap.remove(task.getId());
            }
        }
    }

    public String getTaskIdByNumber(int n, List<Task> filteredListOfTasks) {
        for (Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(n - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }
}

