package ru.rastorguev.tm.controller;

import static ru.rastorguev.tm.view.View.*;
import ru.rastorguev.tm.enumerated.Command;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandController {

    private ProjectController projectController;
    private TaskController taskController;

    public CommandController(BufferedReader reader) {
        this.projectController = new ProjectController(reader);
        this.taskController = new TaskController(projectController, reader);
    }

    public void commandExecution (Command command) throws IOException {
        switch (command) {
            case HELP:
                showHelpMsg();
                break;
            case PROJECT_CREATE:
                projectController.createProject();
                break;
            case PROJECT_LIST:
                projectController.listProjects();
                break;
            case PROJECT_SELECT:
                projectController.selectProject();
                break;
            case PROJECT_EDIT:
                projectController.editProject();
                break;
            case PROJECT_REMOVE:
                projectController.removeProject();
                break;
            case PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TASK_CREATE:
                taskController.createTask();
                break;
            case TASK_LIST:
                taskController.listTasks();
                break;
            case TASK_SELECT:
                taskController.selectTask();
                break;
            case TASK_EDIT:
                taskController.editTask();
                break;
            case TASK_REMOVE:
                taskController.removeTask();
                break;
            case TASK_CLEAR:
                taskController.clearProjectTasks();
                break;
            case UNKNOWN:
                showUnknownCommandMsg();
                break;
        }
    }
}
