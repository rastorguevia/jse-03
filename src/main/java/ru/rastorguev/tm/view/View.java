package ru.rastorguev.tm.view;

public class View {

    public View() {
    }

    public static void showWelcomeMsg() {
        System.out.println("* TASK MANAGER * \n" +
                "* help - show all commands * \n" +
                "* command case is not important *");
    }

    public static void showUnknownCommandMsg() {
        System.out.println("Unknown Command \n" +
                "try again");
    }

    public static void showHelpMsg() {
        System.out.println("project_create: Create new project.\n" +
                "project_list: Show all projects.\n" +
                "project-select: Select exact project\n" +
                "project_edit: Edit selected project\n" +
                "project_remove: Remove selected project\n" +
                "project_clear: Remove all projects.\n\n" +
                "task_create: Create new task.\n" +
                "task_list: Show all tasks.\n" +
                "task_edit: Edit selected task.\n" +
                "task_remove: Remove selected task.\n" +
                "task_clear: Remove  project tasks.");
    }
}
